//
//  ViewController.swift
//  WorldTrotter
//
//  Created by Jay on 01/06/17.
//  Copyright © 2017 the chalakas. All rights reserved.
//

import UIKit

class ViewController: UIViewController
{
    
    
    /*
     
     //this was about creating views programmatically. We dont need that anymore, so commenting it out.

    //the moment viewcontrollers' view is on memory, the method it calls will be viewDidLoad
    //so lets overload that so we can load the views we want to be loaded.
    override func viewDidLoad()
    {
        super.viewDidLoad()
        //its still odd that there is no semi colon in the swift language. waaaat!!!
        //lets create a frame first
        let firstFrame = CGRect(x: 160, y: 240, width: 100, height: 150)
        //create the view using the above frame.
        let firstView = UIView(frame: firstFrame)
        //set the background color
        //that rab ne bana de jodi movie means yellow
        firstView.backgroundColor = UIColor.yellow
        //add the above newly created view as the subview of the current view, which is the main window
        //which itself is a view
        view.addSubview(firstView)
        
        //okay, repeating the above create another view
        let secondFrame = CGRect(x: 20, y: 240, width: 50, height: 50)
        //create the view using the above frame.
        let secondView = UIView(frame: secondFrame)
        //set the background color
        //that rab ne bana de jodi movie means yellow
        secondView.backgroundColor = UIColor.yellow
        //add the above newly created view as the subview of the current view, which is the main window
        //which itself is a view
        //view.addSubview(secondView)
        
        //let me make the secondView the sub view of the first view
        firstView.addSubview(secondView)
        
        
        
    }
 
     */

}

